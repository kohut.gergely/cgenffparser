from __future__ import annotations
from typing import List


class Atom:

    def __init__(self, name: str, index: int, _type: str, charge: float, penalty: float = None):
        self.name = name
        self.index = index
        self.type = _type
        self.charge = charge
        self.penalty = penalty
        self.bonds = []
        self.impropers = []

    def __repr__(self) -> str:
        return f'{self.name:3s}'

    def __str__(self) -> str:
        return f'ATOM {self.name:4s}\t{self.type:6s}\t{self.charge:6.2f} ! penalty = {self.penalty}'

    def __eq__(self, other) -> bool:
        if isinstance(other, Atom):
            if self.name == other.name:
                return True
        return False


class Bond:

    def __init__(self, atom1: Atom, atom2: Atom, param: BondParam = None):

        self.atom1 = atom1
        self.atom2 = atom2
        self.atoms = [atom1, atom2]
        self.param = param

    def __repr__(self) -> str:
        return f'{self.atom1.name}-{self.atom2.name}'

    def __str__(self) -> str:
        name = 'BOND'
        return f'{name:5s}{self.atom1.name:4s}{self.atom2.name:4s}'

    def __getitem__(self, item: int) -> Atom:
        return self.atoms[item]

    def partner(self, atom: Atom) -> Atom:
        """Bond.partner(Atom)
        Returns
        -------
        the other :class:`~MDAnalysis.core.groups.Atom` in this
        bond
        """
        if atom == self.atoms[0]:
            return self.atoms[1]
        elif atom == self.atoms[1]:
            return self.atoms[0]
        else:
            raise ValueError("Unrecognised Atom")

    def reverse(self) -> Bond:
        self.atom1, self.atom2 = self.atom2, self.atom1
        self.atoms = self.atoms[::-1]
        return self


class Angle:

    def __init__(self, atom1: Atom, atom2: Atom, atom3: Atom, param: AngleParam = None):
        self.atom1 = atom1
        self.atom2 = atom2
        self.atom3 = atom3
        self.atoms = [atom1, atom2, atom3]
        self.param = param

    def __repr__(self) -> str:
        return f'{self.atom1.name}-{self.atom2.name}-{self.atom3.name}'

    def __str__(self) -> str:
        name = 'ANGLE'
        return f'{name:6s}{self.atom1.name:3s}\t{self.atom2.name:3s}\t{self.atom3.name:3s}'

    def __getitem__(self, item: int) -> Atom:
        return self.atoms[item]

    def reverse(self) -> Angle:
        self.atom1, self.atom3 = self.atom3, self.atom1
        self.atoms = self.atoms[::-1]
        return self


class Dihedral:

    def __init__(self, atom1: Atom, atom2: Atom, atom3: Atom, atom4: Atom, param: DihedralParam = None):
        self.atom1 = atom1
        self.atom2 = atom2
        self.atom3 = atom3
        self.atom4 = atom4
        self.atoms = [atom1, atom2, atom3, atom4]
        self.param = param

    def __repr__(self) -> str:
        return f'{self.atom1.name}-{self.atom2.name}-{self.atom3.name}-{self.atom4.name}'

    def __str__(self) -> str:
        name = 'DIHEDRAL'
        return f'{name:9s}{self.atom1.name:3s}\t{self.atom2.name:3s}\t{self.atom3.name:3s}\t{self.atom4.name:3s}'

    def __getitem__(self, item: int) -> Atom:
        return self.atoms[item]

    def reverse(self) -> Dihedral:
        self.atom1, self.atom2, self.atom3, self.atom4 = self.atom4, self.atom3, self.atom2, self.atom1
        self.atoms = self.atoms[::-1]
        return self


class Improper:

    def __init__(self, atom1: Atom, atom2: Atom, atom3: Atom, atom4: Atom, param: ImproperParam = None):
        self.atom1 = atom1
        self.atom2 = atom2
        self.atom3 = atom3
        self.atom4 = atom4
        self.atoms = [atom1, atom2, atom3, atom4]
        self.param = param

    def __repr__(self) -> str:
        return f'{self.atom1.name}-{self.atom2.name}-{self.atom3.name}-{self.atom4.name}'

    def __str__(self) -> str:
        name = 'IMPR'
        return f'{name:5s}{self.atom1.name:4s}{self.atom2.name:4s}{self.atom3.name:4s}{self.atom4.name:4s}'

    def __getitem__(self, item: int) -> Atom:
        return self.atoms[item]


class BondParam:

    def __init__(self, atom_type1: str, atom_type2: str, k: float, r0: float, penalty: float = None,
                 bonds: List[Bond] = None):
        self.type1 = atom_type1
        self.type2 = atom_type2
        self.k = k
        self.r0 = r0
        self.penalty = penalty
        self._bonds = bonds

    def __str__(self) -> str:
        if not self.penalty:
            self.penalty = 0.0
        return f'{self.type1:4s}\t{self.type2:4s}\t{self.k:6.2f}' \
               f'\t{self.r0:5.4f} ! penalty = {self.penalty:4.2f}'

    @property
    def bonds(self) -> List[Bond]:
        if self._bonds is None:
            self._bonds = []
        return self._bonds

    def add_bond(self, bond: Bond):
        if self._bonds is None:
            self._bonds = []
        if isinstance(bond, Bond):
            self._bonds.append(bond)
        else:
            raise TypeError(f'expect type Bond got {type(bond)}!')


class AngleParam:

    def __init__(self, atom_type1: str, atom_type2: str, atom_type3: str, k: float, theta0: float,
                 penalty: float = None, angles: List[Angle] = None):
        self.type1 = atom_type1
        self.type2 = atom_type2
        self.type3 = atom_type3
        self.k = k
        self.theta0 = theta0
        self.penalty = penalty
        self._angles = angles

    def __str__(self) -> str:
        if not self.penalty:
            self.penalty = 0.0
        return f'{self.type1:6s}\t{self.type2:6s}\t{self.type3:6s}\t{self.k:4.2f}\t' \
               f'{self.theta0:4.2f} ! penalty = {self.penalty:4.2f}'

    @property
    def angles(self) -> List[Angle]:
        if not self._angles:
            self._angles = []
        return self._angles

    def add_angle(self, angle: Angle):
        if not self._angles:
            self._angles = []
        if isinstance(angle, Angle):
            self._angles.append(angle)
        else:
            raise TypeError(f'expect type Angle got {type(angle)}!')


class DihedralParam:

    def __init__(self, atom_type1: str, atom_type2: str, atom_type3: str, atom_type4: str, k: float, mult: int,
                 delta: float, penalty: float = None, dihedrals: List[Dihedral] = None):
        self.type1 = atom_type1
        self.type2 = atom_type2
        self.type3 = atom_type3
        self.type4 = atom_type4
        self.k = k
        self.mult = mult
        self.delta = delta
        self.penalty = penalty
        self._dihedrals = dihedrals

    def __str__(self) -> str:
        if not self.penalty:
            self.penalty = 0.0
        return f'{self.type1:6s}\t{self.type2:6s}\t{self.type3:6s}\t{self.type4:6s}\t' \
               f'{self.k:4.2f}\t{self.mult:4d}\t{self.delta:4.2f} ! penalty = {self.penalty:4.2f}'

    @property
    def dihedrals(self) -> List[Dihedral]:
        if not self._dihedrals:
            self._dihedrals = []
        return self._dihedrals

    def add_dihedral(self, dihedral: Dihedral):
        if not self._dihedrals:
            self._dihedrals = []
        if isinstance(dihedral, Dihedral):
            self._dihedrals.append(dihedral)
        else:
            raise TypeError(f'expect type Dihedral got {type(dihedral)}!')


class ImproperParam:

    def __init__(self, atom_type1: str, atom_type2: str, atom_type3: str, atom_type4: str, k: float, mult: int,
                 delta: float, penalty: float = None, impropers: List[Improper] = None):
        self.type1 = atom_type1
        self.type2 = atom_type2
        self.type3 = atom_type3
        self.type4 = atom_type4
        self.k = k
        self.mult = mult
        self.delta = delta
        self.penalty = penalty
        self._impropers = impropers

    def __str__(self) -> str:
        if not self.penalty:
            self.penalty = 0.0
        return f'{self.type1:6s}\t{self.type2:6s}\t{self.type3:6s}\t{self.type4:6s}\t' \
               f'{self.k:4.2f}\t{self.mult:4d}\t{self.delta:4.2f} ! penalty = {self.penalty:4.2f}'

    def add_improper(self, improper: Improper):
        if not self._impropers:
            self._impropers = []
        if isinstance(improper, Improper):
            self._impropers.append(improper)
        else:
            raise TypeError(f'expect type Improper got {type(improper)}!')







