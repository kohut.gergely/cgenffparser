from __future__ import annotations
import re
from itertools import count
import argparse as arg
from core_objects import *


class CgenffStrParser:
    """CGenFF .str file parser. It reads and parses a .str file generated by CgenFF and also couples the parameters
        to the related bonds, angles and dihedrals based on the atom names. Tested with program version 2.4.0
        and CGenFF version 4.4. The following parameters are parsed and stored:
        molecule name and charge [line starts with RESI] -> self.resname, self.charge
        Atoms: types, charges and penalties [lines start with ATOM] --> self.atoms
        Bonds: [lines start with BOND] -> self.bonds
        Angles: guessed based on the bonding using MDAnalysis algorithm -> self.angles
        Dihedrals: guessed based on the bonding using MDAnalysis algorithm -> self.dihedrals
        Improper angles: [lines start with IMPR] / guessing also available using MDAnalysis algorithm -> self.impropers
        Bond parameter: atom types, k, r0, and penalties [under BONDS directive] -> self.bond_params
        Angle parameter: atom types, k, r0, and penalties [under BONDS directive] -> self.angle_params
        Dihedral angle parameter: atom types, k, multiplicity, delta, penalty -> self.dihedral_params
        Improper angle parameter: atom types, k, multiplicity, delta, penalty -> self.improper_params
         """

    def __init__(self, filename: str):
        self.resname = None
        self.total_charge = None
        self.original_str = []
        self.header = None
        self.atoms = []
        self.bonds = []
        self.angles = []
        self.dihedrals = []
        self.impropers = []
        self.bond_params = []
        self.angle_params = []
        self.dihedral_params = []
        self.improper_params = []
        self._penalty_threshold = 0
        self.str_reader(filename)

    @property
    def penalty_threshold(self) -> float:
        return self._penalty_threshold

    @penalty_threshold.setter
    def penalty_threshold(self, value: float):
        """
        Penalty threshold for parameter listing can be set with this property!
        Args:
            value: float

        Returns:

        """
        if value >= 0:
            self._penalty_threshold = value
        else:
            raise ValueError('Penalty must be zero/positive!')

    @staticmethod
    def penalty(line: str) -> float:
        """
        Returns the penalty of a given parameter based on its line! Three different variations are supported:
           1. Penalty score is placed right after the ! in the corresponding line
           2. Penalty score is placed after 'penalty= '
           3. Penalty score is placed after 'PENALTY= '

        Args:
            line: string, line of the parameter in the file

        Returns: float, penalty

        """

        if '!' in line:
            try:
                return float(re.findall("!\s+(\d*\.\d+|\d+)", line)[0])
            except IndexError:
                try:
                    return float(re.findall(r"penalty= (\d*\.\d+|\d+)", line)[0])
                except IndexError:
                    try:
                        return float(re.findall(r"PENALTY= (\d*\.\d+|\d+)", line)[0])
                    except IndexError:
                        return None

    def get_atom_by_index(self, index: int) -> Atom:
        """
        Finds an atom based on its index
        Args:
            index: integer, index

        Returns: Atom
        """

        for atom in self.atoms:
            if atom.index == index:
                return atom
        raise IndexError(f'Atom with index {index} could not found')

    def str_reader(self, filename: str):
        """
        Main method to read the CGenFF .str file.
        Args:
            filename: string, file name

        Returns:

        """

        counter = count()
        with open(filename) as f:
            while True:
                try:
                    line = next(f)
                    self.original_str.append(line)

                    if line.startswith('RESI '):
                        spltline = line.split()
                        self.resname = spltline[1]
                        self.header = "".join(self.original_str[:-1])

                    if line.startswith('ATOM'):
                        spltline = line.split()
                        name, _type, charge = spltline[1], spltline[2], float(spltline[3])
                        penalty = self.penalty(line)
                        self.atoms.append(Atom(name, next(counter), _type, charge, penalty))

                    if line.startswith('BOND '):
                        spltline = line.split()
                        atom1 = [atom for atom in self.atoms if atom.name == spltline[1]][0]
                        atom2 = [atom for atom in self.atoms if atom.name == spltline[2]][0]
                        new_bond = Bond(atom1, atom2)
                        atom1.bonds.append(new_bond)
                        atom2.bonds.append(new_bond)
                        self.bonds.append(new_bond)

                    if line.startswith('IMPR '):
                        spltline = line.split()
                        atom1 = [atom for atom in self.atoms if atom.name == spltline[1]][0]
                        atom2 = [atom for atom in self.atoms if atom.name == spltline[2]][0]
                        atom3 = [atom for atom in self.atoms if atom.name == spltline[3]][0]
                        atom4 = [atom for atom in self.atoms if atom.name == spltline[4]][0]
                        new_impr = Improper(atom1, atom2, atom3, atom4)
                        atom1.impropers.append(new_impr)
                        atom2.impropers.append(new_impr)
                        atom3.impropers.append(new_impr)
                        atom4.impropers.append(new_impr)
                        self.impropers.append(new_impr)

                    if line.startswith('BONDS'):
                        line = next(f)
                        while not line.startswith('\n'):
                            spltline = line.split()
                            new_bondparam = BondParam(spltline[0], spltline[1], float(spltline[2]), float(spltline[3]),
                                                      penalty=self.penalty(line))  # save bond parameter record
                            bonds = [bond for bond in self.bonds
                                    if (bond.atom1.type == new_bondparam.type1
                                        and bond.atom2.type == new_bondparam.type2)]
                            bonds.extend([bond.reverse() for bond in self.bonds
                                    if (bond.atom1.type == new_bondparam.type2
                                        and bond.atom2.type == new_bondparam.type1)])
                            bonds = list(set(bonds))
                            for bond in bonds:
                                bond.param = new_bondparam
                                new_bondparam.add_bond(bond)
                            self.bond_params.append(new_bondparam)
                            line = next(f)
                        self.bond_params = tuple(self.bond_params)

                    if line.startswith('ANGLES'):
                        self.guess_angles()
                        line = next(f)
                        while not line.startswith('\n'):
                            spltline = line.split()
                            new_angleparam = AngleParam(spltline[0], spltline[1], spltline[2],
                                                        float(spltline[3]), float(spltline[4]),
                                                        penalty=self.penalty(line))  # save bond parameter record
                            angles = [angle for angle in self.angles
                                    if (angle.atom1.type == new_angleparam.type1
                                        and angle.atom2.type == new_angleparam.type2
                                        and angle.atom3.type == new_angleparam.type3)]
                            angles.extend([angle.reverse() for angle in self.angles
                                    if angle.atom1.type == new_angleparam.type3
                                        and angle.atom2.type == new_angleparam.type2
                                        and angle.atom3.type == new_angleparam.type1])
                            angles = list(set(angles))
                            for angle in angles:
                                angle.param = new_angleparam
                                new_angleparam.add_angle(angle)
                            self.angle_params.append(new_angleparam)  # appending only once
                            line = next(f)
                        self.angle_params = tuple(self.angle_params)

                    if line.startswith('DIHEDRALS'):
                        self.guess_dihedrals()
                        line = next(f)
                        while not line.startswith('\n'):
                            spltline = line.split()
                            new_dihedparam = DihedralParam(spltline[0], spltline[1], spltline[2], spltline[3],
                                                           float(spltline[4]), int(spltline[5]), float(spltline[6]),
                                                           penalty=self.penalty(line))  # save bond parameter record
                            angles = [angle for angle in self.dihedrals
                                    if angle.atom1.type == new_dihedparam.type1
                                        and angle.atom2.type == new_dihedparam.type2
                                        and angle.atom3.type == new_dihedparam.type3
                                        and angle.atom4.type == new_dihedparam.type4]
                            angles.extend([angle.reverse() for angle in self.dihedrals
                                    if angle.atom1.type == new_dihedparam.type4
                                        and angle.atom2.type == new_dihedparam.type3
                                        and angle.atom3.type == new_dihedparam.type2
                                        and angle.atom4.type == new_dihedparam.type1])
                            angles = list(set(angles))
                            for angle in angles:
                                angle.param = new_dihedparam
                                new_dihedparam.add_dihedral(angle)
                            self.dihedral_params.append(new_dihedparam)
                            line = next(f)
                        self.dihedral_params = tuple(self.dihedral_params)

                    if line.startswith('IMPROPERS'):
                        #self.guess_improper_dihedrals()
                        line = next(f)
                        while not line.startswith('\n'):
                            spltline = line.split()
                            new_imparam = ImproperParam(spltline[0], spltline[1], spltline[2], spltline[3],
                                                           float(spltline[4]), int(spltline[5]), float(spltline[6]),
                                                           penalty=self.penalty(line))  # save bond parameter record
                            angles = [angle for angle in self.impropers
                                    if angle.atom1.type == new_imparam.type1
                                        and angle.atom2.type == new_imparam.type2
                                        and angle.atom3.type == new_imparam.type3
                                        and angle.atom4.type == new_imparam.type4]
                            angles.extend([angle.reverse() for angle in self.impropers
                                    if angle.atom1.type == new_imparam.type4
                                        and angle.atom2.type == new_imparam.type3
                                        and angle.atom3.type == new_imparam.type2
                                        and angle.atom4.type == new_imparam.type1])
                            angles = list(set(angles))
                            for angle in angles:
                                angle.param = new_imparam
                                new_imparam.add_improper(angle)
                            self.improper_params.append(new_imparam)
                            line = next(f)
                        self.improper_params = tuple(self.improper_params)

                except StopIteration:
                    self.total_charge = sum([atom.charge for atom in self.atoms])
                    break

    # From MDAnalysis
    def guess_angles(self):
        """Given a list of Bonds, find all angles that exist between atoms.
        Works by assuming that if atoms 1 & 2 are bonded, and 2 & 3 are bonded,
        then (1,2,3) must be an angle.
        Returns
        """
        angles_found = set()
        for b in self.bonds:
            for atom in b:
                other_a = b.partner(atom)  # who's my friend currently in Bond
                for other_b in atom.bonds:
                    if other_b != b:  # if not the same bond I start as
                        third_a = other_b.partner(atom)
                        desc = tuple([other_a.index, atom.index, third_a.index])
                        if desc[0] > desc[-1]:  # first index always less than last
                            desc = desc[::-1]
                        angles_found.add(desc)

        for desc in tuple(angles_found):
            angle = []
            for index in desc:
                angle.append(self.get_atom_by_index(index))
            self.angles.append(Angle(*angle))

    # From MDAnalysis
    def guess_dihedrals(self):
        """Given a list of Angles, find all dihedrals that exist between atoms.
        Works by assuming that if (1,2,3) is an angle, and 3 & 4 are bonded,
        then (1,2,3,4) must be a dihedral.
        Returns:
        """
        dihedrals_found = set()
        for b in self.angles:
            a_tup = tuple([a.index for a in b])  # angle as tuple of numbers
            # if searching with b[0], want tuple of (b[2], b[1], b[0], +new)
            # search the first and last atom of each angle
            for atom, prefix in zip([b.atoms[0], b.atoms[-1]],
                                    [a_tup[::-1], a_tup]):
                for other_b in atom.bonds:
                    if not other_b.partner(atom) in b:
                        third_a = other_b.partner(atom)
                        desc = prefix + (third_a.index,)
                        if desc[0] > desc[-1]:
                            desc = desc[::-1]
                        dihedrals_found.add(desc)

        for desc in tuple(dihedrals_found):
            dihedral = []
            for index in desc:
                dihedral.append(self.get_atom_by_index(index))
            self.dihedrals.append(Dihedral(*dihedral))

    def guess_improper_dihedrals(self):
        """Given a list of Angles, find all improper dihedrals that exist between
        atoms.

        Works by assuming that if (1,2,3) is an angle, and 2 & 4 are bonded,
        then (2, 1, 3, 4) must be an improper dihedral.
        ie the improper dihedral is the angle between the planes formed by
        (1, 2, 3) and (1, 3, 4)
        """
        impropers_found = set()
        for b in self.angles:
            atom = b[1]  # select middle atom in angle
            # start of improper tuple
            a_tup = tuple([b[a].index for a in [1, 2, 0]])
            # if searching with b[1], want tuple of (b[1], b[2], b[0], +new)
            # search the first and last atom of each angle
            for other_b in atom.bonds:
                other_atom = other_b.partner(atom)
                # if this atom isn't in the angle I started with
                if not other_atom in b:
                    desc = a_tup + (other_atom.index,)
                    if desc[0] > desc[-1]:
                        desc = desc[::-1]
                    impropers_found.add(desc)

        for desc in tuple(impropers_found):
            improper = []
            for index in desc:
                improper.append(self.get_atom_by_index(index))
            self.impropers.append(Improper(*improper))

    def change_atom_type(self, atom_name: str, new_type: str):
        """
        The type of an atom can be changed with this method by giving the name and new type of the atom to be modified.
        Args:
            atom_name: string, precise name of the atom to be changed!
            new_type: string, precise name of the new type of the atom!

        Returns:

        """

        for atom in self.atoms:
            if atom.name == atom_name:
                atom.type = new_type

    def write_stream_file(self, output_filename : str = None):
        """
        Writes the new CGenFF stream file.
        Args:
            output_filename: string, filename of the output file

        Returns:

        """
        if not output_filename:
            output_filename = f'{self.resname}_modified.str'
        with open(output_filename, 'w') as f:
            f.write(str(self.header))
            f.write(f'RESI {self.resname}\t{self.total_charge:6.4f}\n')
            f.write('GROUP\n')
            for item in self.atoms:
                f.write(str(item) + '\n')
            f.write('\n')
            for item in self.bonds:
                f.write(str(item) + '\n')
            for item in self.impropers:
                f.write(str(item) + '\n')
            f.write('\nEND\n')
            f.write('\nread param card flex append\n')
            f.write('\nBONDS\n')
            for item in self.bond_params:
                f.write(f'{str(item)}; BONDS: {[element.__repr__() for element in item.bonds]}\n')
            f.write('\nANGLES\n')
            for item in self.angle_params:
                f.write(f'{str(item)}; ANGLES: {[element.__repr__() for element in item.angles]}\n')
            f.write('\nDIHEDRALS\n')
            for item in self.dihedral_params:
                f.write(f'{str(item)}; DIHEDRALS: {[element.__repr__() for element in item.dihedrals]}\n')
            f.write('\nIMPROPERS\n')
            f.write('\nEND\n')
            f.write('RETURN')


def main():
    parser = arg.ArgumentParser(description='A script that does atom type-to-name mapping of the CHARMM parameters!')
    parser.add_argument('-f', '--filename', type=str, required=True,
                        help='Input filename! Should be a CHARMM stream file generated by CgenFF.')
    parser.add_argument('-p', '--penalty', type=float, default=0.0,
                        help='Threshold penalty for filtering.')
    parser.add_argument('-o', '--output', type=str, default=None,
                        help='Output file name.')
    args = parser.parse_args()
    x = CgenffStrParser(args.filename)
    x.change_atom_type('C8', 'CGSR4')
    x.penalty_threshold = args.penalty
    x.write_stream_file(args.output)

if __name__ == '__main__':
    x = CgenffStrParser('spiro_init2.str')
    x.change_atom_type('C8', 'CGSR4')
    x.penalty_threshold = 10.0
    x.write_stream_file('spiro_params_10.dat')
    #main()
